import { validationFn } from "./validations";

export const validateForms = () => {
  
  const forms = $("form[validate=true]").toArray();

  forms.forEach((form, index) => {
    
    $(form).attr("novalidate", true);
   
    $(form).attr("data-uid", `form-${index}`);
    validateForm(form);
  });
};

const validateForm = form => {
  
  const inputs = $(form)
    .find("input, textarea, select")
    .toArray();
  validateFields(inputs);
 
  $(form).on("submit", evt => {
    let focusedFirstInvalidInput = false;

    inputs.forEach(input => {
      if (!validateField(input)) {
        // Put the focus on the first invalid input
        if (!focusedFirstInvalidInput) {
          $(input).focus();
          focusedFirstInvalidInput = true;
        }
        evt.preventDefault();
      }
    });
  });
};

const validateFields = inputs => {
  inputs.forEach((input, index) => {
    
    const formUID = $(input)
      .parent("form")
      .attr("data-uid");
    
    let event = "blur";
    
    $(input).attr("data-uid", `${formUID}-field-${index}`);
    if ($(input).attr("type") == "file") {
     
      event = "change";
    }
    $(input).on(event, evt => {
      validateField(input);
    });
  });
};


const validateField = input => {
  let valid = true;

  if ($(input).attr("required")) {
    if ($(input).attr("type") == "checkbox") {
      
      valid = valid
        ? validate(
            input,
            "checked",
            validationFn.required,
            "You need to check this"
          )
        : valid;
    } else {
      valid = valid
        ? validate(
            input,
            "value",
            validationFn.required,
            "This field is required"
          )
        : valid;
    }
  }
  if ($(input).attr("type") == "email") {
    valid = valid
      ? validate(input, "value", validationFn.email, "The email is invalid")
      : valid;
  }
  if ($(input).attr("type") == "url") {
    valid = valid
      ? validate(input, "value", validationFn.url, "The url is invalid")
      : valid;
  }
  if ($(input).attr("type") == "mobile") {
    valid = valid
      ? validate(
          input,
          "value",
          validationFn.mobile,
          "The mobile number is invalid"
        )
      : valid;
  }
  if ($(input).attr("pattern")) {
    valid = valid
      ? validate(
          input,
          "value",
          validationFn.pattern,
          "Please match the requested pattern"
        )
      : valid;
  }
  if ($(input).attr("data-match-field")) {
    valid = valid
      ? validate(
          input,
          "value",
          validationFn.match,
          `${$(input).attr("data-match")} fields does not match`
        )
      : valid;
  }
  if ($(input).attr("type") == "file") {
    if ($(input).attr("data-file-types")) {
      valid = valid
        ? validate(
            input,
            "",
            validationFn.fileTypes,
            `Invalid file type selected`
          )
        : valid;
    }
    if ($(input).attr("data-file-max-size")) {
      const maxSize = $(input).attr("data-file-max-size");

      valid = valid
        ? validate(
            input,
            "",
            validationFn.fileMaxSize,
            `File size must be < ${maxSize}`
          )
        : valid;
    }
    if ($(input).attr("data-file-min-size")) {
      const maxSize = $(input).attr("data-file-min-size");

      valid = valid
        ? validate(
            input,
            "",
            validationFn.fileMinSize,
            `File size must be > ${maxSize}`
          )
        : valid;
    }
  }
  if ($(input).attr("type") == "number") {
    valid = valid
      ? validate(input, "value", validationFn.isNumber, "This is not a number")
      : valid;
    if ($(input).attr("max")) {
      const maxValue = $(input).attr("max");

      valid = valid
        ? validate(
            input,
            "value",
            validationFn.max,
            `This should be < ${maxValue}`
          )
        : valid;
    }
    if ($(input).attr("min")) {
      const minValue = $(input).attr("min");

      valid = valid
        ? validate(
            input,
            "value",
            validationFn.min,
            `This should be > ${minValue}`
          )
        : valid;
    }
  }
  if ($(input).attr("maxlength")) {
    const maxLength = $(input).attr("maxlength");

    valid = valid
      ? validate(
          input,
          "value",
          validationFn.maxLength,
          `This should be less than ${maxLength} character(s)`
        )
      : valid;
  }
  if ($(input).attr("minlength")) {
    const minLength = $(input).attr("minlength");

    valid = valid
      ? validate(
          input,
          "value",
          validationFn.minLength,
          `This should be atleast ${minLength} character(s)`
        )
      : valid;
  }

  return valid;
};


const validate = (input, property, validation, errorMessage) => {
  const errorContainer = initErrorContainer(input);
  errorMessage = $(input).attr("data-error") || errorMessage;

  if (
    !validation({
      input: input,
      property: property,
      value: $(input).prop(property)
    })
  ) {
    $(input).addClass("error");
    $(input).removeClass("no-error");
    errorContainer.innerHTML = errorMessage;

    return false;
  } else {
    $(input).addClass("no-error");
    $(input).removeClass("error");
    errorContainer.remove();

    return true;
  }
};


const initErrorContainer = input => {
  const inputUID = $(input).attr("data-uid");
  const errorContainerUID = `${inputUID}-error`;
  let errorContainer;

  
  if ($(`#${errorContainerUID}`).length == 0) {
    errorContainer = document.createElement("div");
    errorContainer.setAttribute("id", errorContainerUID);
    errorContainer.className = "error-field";
    $(input).after(errorContainer);
  } else {
    errorContainer = document.getElementById(errorContainerUID);
  }

  return errorContainer;
};
    

$(document).ready(function(){
$('#submit1').click(function(){
var gender=$('#gender').val();

if ($("#gender:checked").length == 0){
$('#dis').slideDown().html('<span id="error">Please choose gender</span>');
return false;
}

});
});

